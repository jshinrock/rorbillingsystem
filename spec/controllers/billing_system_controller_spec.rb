require 'rails_helper'

RSpec.describe BillingSystemController, type: :controller do
 before(:each) do 
    @controller = BillingSystemController.new
  end

  describe "smoke test" do
    it "should runs tests" do
      expect(true).to eq(true)
    end
  end

  it "should generates [n] order from input" do
    order = @controller
    num_orders = rand(100)
    total_orders = order.generate_sample_orders(num_orders)
    expect(total_orders.length).to eq(num_orders)
    total_orders.each { |order|
      expect(order).to be_a WidgetOrder
    }
  end

  it "should fetch a random company" do
    company = @controller.get_rand_company
    expect(company).not_to be(nil)
    expect(company).to have_key(:id)
  end

  describe "utility methods" do
    before(:all) do
      @controller = BillingSystemController.new
      @mock_customers = [
        {
          id: 1,
          name: 'test_1',
        },
        {
          id: 2,
          name: 'test_2'
        },
        {
          id: 3,
          name: 'test_3'
        },
      ]

      @mock_orders = [
        @controller.generate_order(@mock_customers[0], 2),
        @controller.generate_order(@mock_customers[0], 10),
        @controller.generate_order(@mock_customers[1], 5),
        @controller.generate_order(@mock_customers[2], 1),
        @controller.generate_order(@mock_customers[2], 27)
      ]
    end

    it "should group by company orders " do
      grouped_orders = BillingSystemController.new.group_company_orders(@mock_orders)
      expect(grouped_orders.length).to eq(3)
    end

    it "should get total widget count by company " do
      grouped_orders = controller.group_company_orders(@mock_orders)
      company_one_widgets = controller.get_total_widget_sales_by_company(grouped_orders[1])
      company_two_widgets = controller.get_total_widget_sales_by_company(grouped_orders[2])
      company_three_widgets = controller.get_total_widget_sales_by_company(grouped_orders[3])

      expect(company_one_widgets).to eq(12)
      expect(company_two_widgets).to eq(5)
      expect(company_three_widgets).to eq(28)
    end

  end

  describe "calculation rollup reports" do
    before(:each) do
      @controller = BillingSystemController.new

      @mock_customers = [
        {
          id: 0,
          name: 'direct',
          type: 'direct',
          retail_widget_price: 100
        },
        {
          id: 1,
          name: 'affiliate',
          type: 'affiliate',
          retail_widget_price: 75
        },
        {
          id: 2,
          name: 'reseller',
          type: 'reseller',
          retail_widget_price: 80
        }
      ]
    end

    it "should calculate widget price per monthly order for customer types" do
      ### low volume affiliate pricing
      low_volume = @controller.set_widget_price_per_month_by_customer_type("affiliate", 500)      
      ### mid volume affiliate pricing
      mid_volume = @controller.set_widget_price_per_month_by_customer_type("affiliate", 750)
      ### high volume affiliate pricing
      high_volume = @controller.set_widget_price_per_month_by_customer_type("affiliate", 1200)
      ### reseller pricing
      reseller = @controller.set_widget_price_per_month_by_customer_type("reseller", 405)
      ### diredt pricing
      direct = @controller.set_widget_price_per_month_by_customer_type("direct", 21)

      expect(low_volume).to eq(60)
      expect(mid_volume).to eq(50)
      expect(high_volume).to eq(40)
      expect(reseller).to eq(50)
      expect(direct).to eq(100)
    end

    it "should calculate profit for affiliates and resellers per month" do
      order_one_company_one = @controller.generate_order(@mock_customers[0], 25)
      order_two_company_one = @controller.generate_order(@mock_customers[0], 32)

      order_one_company_two = @controller.generate_order(@mock_customers[1], 125)
      order_two_company_two = @controller.generate_order(@mock_customers[1], 32)
      order_three_company_two = @controller.generate_order(@mock_customers[1], 400)

      order_one_company_three = @controller.generate_order(@mock_customers[2], 15)
      order_two_company_three = @controller.generate_order(@mock_customers[2], 60)

      orders = [
        order_one_company_one,
        order_two_company_one,
        order_one_company_two,
        order_two_company_two,
        order_three_company_two,
        order_one_company_three,
        order_two_company_three
      ]

      profit_report = controller.calculate_profit_by_customer_type(orders)

      # expect direct sales was not calced
      expect(profit_report[0][:customer]).to eq(@mock_customers[1])
      # expect total widgets to be sum of all orders per month = 557
      expect(profit_report[0][:total_widgets_sold]).to eq(557)
      # expect gross profit to be retail (75) * total widgets (557) = 41775
      expect(profit_report[0][:gross_profit]).to eq(41775)
      # expect net spend to be affilaite/reseller pricing (50@501-1000) * total widgets (557) = 27850
      expect(profit_report[0][:net_spend]).to eq(27850)
      #expect net profit to be gross_profit (41775) - affiliate/reseller fees (27850) = 13925
      expect(profit_report[0][:net_profit]).to eq(13925)

      expect(profit_report[1][:customer]).to eq(@mock_customers[2])
      # expect total widgets to be sum of all orders per month = 75
      expect(profit_report[1][:total_widgets_sold]).to eq(75)
      # expect gross profit to be retail (80) * total widgets (75) = 6000
      expect(profit_report[1][:gross_profit]).to eq(6000)
      # expect net spend to be affilaite/reseller pricing (50) * total widgets (75) = 3750
      expect(profit_report[1][:net_spend]).to eq(3750)
      #expect net profit to be gross_profit (6000) - affiliate/reseller fees (3750) = 2250
      expect(profit_report[1][:net_profit]).to eq(2250)
    end

    it "should calculate affiliate/reseller billing" do
      order_one_company_one = controller.generate_order(@mock_customers[0], 25)
      order_two_company_one = controller.generate_order(@mock_customers[0], 32)

      order_one_company_two = controller.generate_order(@mock_customers[1], 670)
      order_two_company_two = controller.generate_order(@mock_customers[1], 32)
      order_three_company_two = controller.generate_order(@mock_customers[1], 400)

      order_one_company_three = controller.generate_order(@mock_customers[2], 545)
      order_two_company_three = controller.generate_order(@mock_customers[2], 20)

      orders = [
        order_one_company_one,
        order_two_company_one,
        order_one_company_two,
        order_two_company_two,
        order_three_company_two,
        order_one_company_three,
        order_two_company_three
      ]

      billing_report = controller.calculate_bill_by_customer_type(orders)

      # expect direct sales was not calced
      expect(billing_report[0][:customer]).to eq(@mock_customers[1])
      # expect total widgets to be sum of all orders per month = 1102
      expect(billing_report[0][:total_widgets_sold]).to eq(1102)
      # expect invoice amount to be affilaite/reseller pricing (40@1000+) * total widgets (1102) = 44080
      expect(billing_report[0][:invoice_amount]).to eq(44080)


      expect(billing_report[1][:customer]).to eq(@mock_customers[2])
      # expect total widgets to be sum of all orders per month = 565
      expect(billing_report[1][:total_widgets_sold]).to eq(565)
      # expect invoice amount to be affilaite/reseller pricing (50) * total widgets (75) = 28250
      expect(billing_report[1][:invoice_amount]).to eq(28250)

    end

    it "should generate total revenue by all orders" do
      mock_customers = [
        {
          id: 0,
          name: 'direct',
          type: 'direct',
          retail_widget_price: 100
        },
        {
          id: 1,
          name: 'affiliate_1',
          type: 'affiliate',
          retail_widget_price: 75
        },
        {
          id: 2,
          name: 'affiliate_2',
          type: 'affiliate',
          retail_widget_price: 75
        },
        {
          id: 3,
          name: 'affiliate_3',
          type: 'affiliate',
          retail_widget_price: 75
        },
        {
          id: 4,
          name: 'reselle_1',
          type: 'reseller',
          retail_widget_price: 80
        },
        {
          id: 5,
          name: 'reseller_2',
          type: 'reseller',
          retail_widget_price: 80
        }
      ]

      order_one_company_one = @controller.generate_order(mock_customers[0], 25)
      order_two_company_one = @controller.generate_order(mock_customers[0], 32)

      order_one_company_two = @controller.generate_order(mock_customers[1], 17)
      order_two_company_two = @controller.generate_order(mock_customers[1], 32)
      order_three_company_two = @controller.generate_order(mock_customers[1], 400)

      order_one_company_three = @controller.generate_order(mock_customers[2], 670)
      order_two_company_three = @controller.generate_order(mock_customers[2], 32)
      order_three_company_three = @controller.generate_order(mock_customers[2], 400)

      order_one_company_four = @controller.generate_order(mock_customers[3], 502)
      order_two_company_four = @controller.generate_order(mock_customers[3], 23)

      order_one_company_five = @controller.generate_order(mock_customers[4], 545)
      order_two_company_five = @controller.generate_order(mock_customers[4], 20)

      order_one_company_six = @controller.generate_order(mock_customers[5], 24)
      order_two_company_six = @controller.generate_order(mock_customers[5], 120)

      orders = [
        order_one_company_one,
        order_two_company_one,
        order_one_company_two,
        order_two_company_two,
        order_three_company_two,
        order_one_company_three,
        order_two_company_three,
        order_three_company_three,
        order_one_company_four,
        order_two_company_four,
        order_one_company_five,
        order_two_company_five,
        order_one_company_six,
        order_two_company_six,
      ]

      revenue_report = @controller.calculate_total_revenue(orders)
      expect(revenue_report[0][:revenue_type]).to eq("direct")
      expect(revenue_report[0][:total_units_sold]).to eq(57)
      expect(revenue_report[0][:total_revenue]).to eq(5700)

      expect(revenue_report[1][:revenue_type]).to eq("affiliate")
      expect(revenue_report[1][:total_units_sold]).to eq(2076)
      expect(revenue_report[1][:total_revenue]).to eq(97270)
      
      expect(revenue_report[2][:revenue_type]).to eq("reseller")
      expect(revenue_report[2][:total_units_sold]).to eq(709)
      expect(revenue_report[2][:total_revenue]).to eq(35450)
    end

  end

  describe "renders reporting output to the console" do

    before(:all) do
      @controller = BillingSystemController.new
      @sample_orders = @controller.generate_sample_orders(100)
      puts "...created #{@sample_orders.length} sample orders"
    end

    it "should render billing report" do
      billing_report = controller.calculate_bill_by_customer_type(@sample_orders)
      @controller.render_console_billing_output('Customer Billing Report', billing_report)
    end

    it "should render customer profit report" do
      profit_report = controller.calculate_profit_by_customer_type(@sample_orders)
      @controller.render_console_profit_output('Customer Profit Report', profit_report)
    end

    it "should render company revenue report" do
      revenue_report = @controller.calculate_total_revenue(@sample_orders)
      @controller.render_console_revenue_output('Revenue Report', revenue_report)
    end

  end
end
