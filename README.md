# README FIRST

Billing system example.

## Version info

* Ruby verion: 2.6.3
* Rails version: 6.0.0
* RSpec version: 3.8.2

## Considerations

* As mentioned in my initial conversation with Sundhakar, it's been several years since I've implemented a RoR project. I have significant experience with ActiveRecord ORM platforms, such as Sails (Node.js port of Rails) and Laravel (PHP ORM). That being said, please forgive any oversights invoving Ruby/RoR stylistic best practices. For example, it appears as though Rails controllers are now the preferred place to handle RESTful routing and a second type of "helper" is created to handle static logic. My implementatin does not reflect that, but a production application influenced by more Rails experience would fit the pieces in the correct place.

* In a real-world scenario with a persistance layer, customers would likely come from a storage solution and be a real one-to-many association (Customers have many WidgetOrders). In the absence of a persistance layer, and due to the necessity to randomly generate records, I've opted to mock customer records via a flat map when generating random orders. An abstracted method exists (BillingSystemController::generate_order) to create associated orders in a more production-like manner.

* This library is well unit-tested, but lacks integration tests, as that functionality would need to be defined and implemented before a testing strategy could be determined.

* Similarly, an Invoice record would likely be saved for each billing report at the end of generation. Without knowing the desired specifics of the billing system, I've opted to bill each widget at the max rate per order (regardless of tier) and add a line-item for account credit at the end of the invoice cycle for adjusted rate based on total monthly sales (affiliate program only) in the form of an account credit. It would certainly be possible to derive affiliate tier information as orders are placed, but would add both complexity and latency to the system. I imagine details such as this would be discussed in product planning.

* The output of the this exercise is logged to the console upon running tests. `rspec` will run the unit test suite as well as output the desired reporting from the 100 random records as requrested from this exercise. 

## Extensibility notes

* The library, as written, processes an array of orders. This design choice was intened to provide as much extensibility as possible to the system. Once an array of orders is derived, it can simply be passed to `calculate_bill_by_customer_type`, `calculate_profit_by_customer_type`, or `calculate_total_revenue`. This abstraction should make billing/reporting trivial to add to both existing systems, as well as the ability to exist as RESTful endpoints for distributed, domain-driven development. Examples of queries immediately before the billing step in the pipeline might include: 
	* `WidgetOrders.where("today >= month1 AND today <= month2")` to pull all orders for a given month.
	* `WidgetOrders.joins(:customer).where('customer.name = "SomeCompany"')` would allow for profit/billing reports based on customer name.
	* `WidgetOrders.joins(:customer).where('customer.type = "affiliate")` would allow for report generation of only affiliates parntners. 
	
* Conceptual RESTful integration might look like:


#### placing an order  

```
	#POST /billing/customer/:id

	@customer = Customer.where("id = params[:id]")
	@order = BillingSystemController.generate_order(request.body.widgets)
	...
```
#### bulk invoicing  

```

	#GET /billing/customer/type/:type
	
	@orders = WidgetOrders.joins(:customer).where('customer.type = ?', params[:type])
	@invoice = BillingSystemController.calculate_bill_by_customer_type(@orders)
	...
```

#### revenue reporting  

```

	#GET /billing/reports/:type
	
	date = Date.today
	@orders = WidgetOrders.where('date >= ?', date.beginning_of_month)
	@invoice = BillingSystemController.calculate_total_revenue(@orders)
	...
```
	

## Billing system usage

* Including BillingSystemController will expose three high-level report generation methods. Each method expects an array of WidgetOrders.
	* `calculate_bill_by_customer_type` generates an invoice per customer
	* `calculate_profit_by_customer_type` generates a profit report for each customer based on partnership type
	* `calculate_total_revenue` generates a company revenue report.

## Quickstart
#### Running the test suite will output the billing reports for 100 random orders at the end of the unit tests. Results will be logged to console. Each subsequent run will generate 100 new random records.

* Clone repo
* `bundle install` in project directory
* `rake db:migrate`
* `rspec`



