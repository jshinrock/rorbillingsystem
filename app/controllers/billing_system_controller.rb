class BillingSystemController < ApplicationController
  # only included for logging purposes
  include ActionView::Helpers::NumberHelper

  def render_console_revenue_output(title, data)
    puts ""
    puts "---------------------------"
    puts "#{title}"
    puts "---------------------------"
    puts ""

    data.each { |type| 
      puts "[ Revenue Stream: #{type[:revenue_type]} ]"
      puts "- Widgets sold: #{type[:total_units_sold]}"
      puts "- Total revenue: $#{number_with_delimiter(type[:total_revenue])}"
      puts " "
    }
  end 

  def render_console_profit_output(title, data)
    puts ""
    puts "---------------------------"
    puts "#{title}"
    puts "---------------------------"
    puts ""
    data.each { |customer| 
      puts "[ Customer: #{customer[:customer][:name]} ]"
      puts "- Vendor type: #{customer[:customer][:type]}"
      puts "- Retail price: $#{customer[:customer][:retail_widget_price]}/widget"
      puts "- Widgets sold: #{customer[:total_widgets_sold]} @ $#{customer[:retail_price]}/widget"
      puts "- Widgets tiered rate: $#{customer[:widget_tier]}/widget"
      puts "- Gross profit: $#{number_with_delimiter(customer[:gross_profit])}"
      puts "- Invoice amount: -$#{number_with_delimiter(customer[:net_spend])}"
      puts "- Monthly profit: $#{number_with_delimiter(customer[:net_profit])}"
      puts " "
    }
  end

  def render_console_billing_output(title, data)
    puts ""
    puts "---------------------------"
    puts "#{title}"
    puts "---------------------------"
    puts ""

    data.each { |customer| 
      puts "[ Customer: #{customer[:customer][:name]} ]"
      puts "- Orders count: #{customer[:distinct_orders]}"
      puts "- Vendor type: #{customer[:customer][:type]}"
      puts "- Widgets sold: #{customer[:total_widgets_sold]}"
      puts "- Widget tiered rate: $#{customer[:widget_tier]}/widget"
      if customer[:customer][:type] == "affiliate"
        puts "- Account credit (tier discount): $#{number_with_delimiter(customer[:account_credit])}"
      end
      puts "- Invoice amount: $#{number_with_delimiter(customer[:invoice_amount])}"
      puts " "
    }
  end

  def generate_sample_orders(orders = 100)
    orders = Array.new(orders)
    orders = orders.map { |o|       
      # get a random company for the order
      customer = self.get_rand_company()
      # also randomize widgets for this order
      widget_orders = rand(100)
      self.generate_order(customer, widget_orders)
    }
    orders
  end

  # NOTE: abstracting this method should allow us to explicitly create an order for a customer
  def generate_order(customer, widget_orders)
    order = WidgetOrder.new
    # as mentioned in considerations section of README, this would almost certainly be an association in a real world scenario
    # ex: order = Customer.WidgetOrders.create!({...})
    order.customer = customer
    order.widget_orders = widget_orders
    order.amount_paid = self.set_widget_price_per_month_by_customer_type(customer[:type], 0) * widget_orders
    # could be paramaterized if required by application
    order.date = Time.now
    order
  end

  def get_rand_company
    companies = [
      {
        id: 0,
        name: 'Direct Sale',
        type: 'direct',
        retail_widget_price: 100
      },
      {
        id: 1,
        name: 'ACompany',
        type: 'affiliate',
        retail_widget_price: 75
      },
      {
        id: 2,
        name: 'AnotherCompany',
        type: 'affiliate',
        retail_widget_price: 65
      },
      {
        id: 3,
        name: 'EvenMoreCompany',
        type: 'affiliate',
        retail_widget_price: 80
      },
      {
        id: 4,
        name: 'ResellThis',
        type: 'reseller',
        retail_widget_price: 75
      },
      {
        id: 5,
        name: 'SellMoreThings',
        type: 'reseller',
        retail_widget_price: 85
      },
    ];

    companies[rand(companies.length)]
  end

  def get_total_widget_sales_by_company(company_orders)
    company_orders.reduce(0) { |memo, order|
      memo + order.widget_orders
    }
  end

  def group_company_orders(orders)
    company_orders = orders.group_by { |order|
      order.customer[:id]
    }
    company_orders
  end

  def set_widget_price_per_month_by_customer_type(company_type, total_orders_by_month)
    case company_type
    when "affiliate"
      get_affiliate_pricing_by_widget_count(total_orders_by_month)
    when "reseller"
      50
    when "direct"
      100
    else
      0
    end
  end

  def get_invoice_tiered_credit(customer_type, total_widget_orders)
    case customer_type
    when "affiliate"
      self.get_affiliate_tiered_credit(total_widget_orders)
    else
      0
    end
  end

  def get_affiliate_tiered_credit(total_widgets_sold)
    case total_widgets_sold
    when 0..500
      0
    when 501..1000
      10
    else
      20
    end
  end

  def get_affiliate_pricing_by_widget_count(widget_count)
    case widget_count
    when 0..500
      60
    when 501..1000
      50
    else
      40
    end
  end

  def filter_direct_sales(orders_by_company)
    orders_by_company.reject { |id, orders| 
      customer = orders.first.customer
      customer[:type] === "direct"
    }
  end

  def calculate_profit_by_customer_type(orders)
    orders_by_company = group_company_orders(orders)
    # we don't calc profit for direct sales
    orders_by_company = self.filter_direct_sales(orders_by_company)
    orders_by_company = orders_by_company.map { |id, orders| 
      customer = orders.first.customer
      type = customer[:type]
      total_widget_orders = self.get_total_widget_sales_by_company(orders)
      gross_profit = customer[:retail_widget_price] * total_widget_orders
      widget_tier = self.set_widget_price_per_month_by_customer_type(type, total_widget_orders) 
      net_spend = widget_tier * total_widget_orders
      net_profit = gross_profit - net_spend

      {
        :customer => customer,
        :total_widgets_sold => total_widget_orders,
        :gross_profit => gross_profit,
        :net_spend => net_spend,
        :net_profit => net_profit,
        :widget_tier => widget_tier,
        :retail_price => customer[:retail_widget_price]
      }
    }

    orders_by_company
  end

  def calculate_bill_by_customer_type(orders)
    orders_by_company = group_company_orders(orders)
    # we don't bill for direct sales
    orders_by_company = self.filter_direct_sales(orders_by_company)
    orders_by_company = orders_by_company.map { |id, orders| 
      customer = orders.first.customer
      total_widget_orders = self.get_total_widget_sales_by_company(orders)
      widget_tier = self.set_widget_price_per_month_by_customer_type(customer[:type], total_widget_orders) 
      invoice_amount = widget_tier * total_widget_orders
      distinct_orders = orders.length
      account_credit = self.get_invoice_tiered_credit(customer[:type], total_widget_orders) * total_widget_orders

      {
        :customer => customer,
        :total_widgets_sold => total_widget_orders,
        :invoice_amount => invoice_amount,
        :distinct_orders => distinct_orders,
        :widget_tier => widget_tier,
        :account_credit => account_credit
      }
    }

    # this would be a logical time to save an invoice record in a production scenario.
    # ex: orders_by_company.each { |invoice| Customer.Invoice.create!({...}) }

    orders_by_company
  end

  def calculate_total_revenue(orders)
    orders_by_company = group_company_orders(orders)
    orders_by_company = orders_by_company.map { |id, orders|
      customer = orders.first.customer
      type = customer[:type]
      total_widget_orders = self.get_total_widget_sales_by_company(orders)
      total_revenue = self.set_widget_price_per_month_by_customer_type(type, total_widget_orders) * total_widget_orders

      {
        :customer => customer,
        :type => type,
        :total_widgets_sold => total_widget_orders,
        :total_revenue => total_revenue,
      }
    }

    revenue_by_type = orders_by_company.group_by { |order| 
      order[:type]
    }

    revenue_by_type = revenue_by_type.map{ |type, data| 
      total_widgets_sold_of_customer_type = data.reduce(0) { |memo, order| 
        memo + order[:total_widgets_sold]
      }
      total_revenue_by_sale_type = data.reduce(0) { |memo, order|
        memo + order[:total_revenue]
      }

      {
        :revenue_type => type,
        :total_units_sold => total_widgets_sold_of_customer_type,
        :total_revenue => total_revenue_by_sale_type
      }
    }

    total_rollup = {
      :revenue_type => 'all streams rollup',
      :total_units_sold => revenue_by_type.reduce(0) { |memo, type| memo + type[:total_units_sold] },
      :total_revenue => revenue_by_type.reduce(0) { |memo, type| memo + type[:total_revenue] },
    }

    revenue_by_type.push(total_rollup)

    revenue_by_type
  end
end
