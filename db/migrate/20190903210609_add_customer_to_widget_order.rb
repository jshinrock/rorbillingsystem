class AddCustomerToWidgetOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :widget_orders, :customer, :string
    add_column :widget_orders, :widget_orders, :int
    add_column :widget_orders, :date, :date
  end
end
