class AddAmountPaidToWidgetOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :widget_orders, :amount_paid, :int
  end
end
